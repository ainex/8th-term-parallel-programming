#include "math.h"
#include "stdio.h"
#include <time.h>

int main(int argc, char **argv) {

    printf("For any given integer N it finds next integer M,\nso that the sum of the squares of its decimal digits is the full square\n");

    printf("Enter an integer N:\n");

    unsigned long long number = 0;

    scanf("%llu", &number);
    printf("N:%llu\n", number);

    time_t begin = time(NULL);

    int notFound = 1;

    while (notFound) {
        number++;
        //find the sum of the squares of digits
        unsigned long long sum = 0;
        int digit = 0;

        unsigned long long dividend = number;

        while (dividend > 0) {
            digit = dividend % 10;
            sum += digit * digit;
            dividend = dividend / 10;

        }

        unsigned long long roundedRoot = (unsigned long long) sqrtl(sum);

        if (roundedRoot * roundedRoot == sum) {
            notFound = 0;
            printf("root K: %llu\n", roundedRoot);
        };

    }

    time_t end = time(NULL);
    // calculate elapsed time by finding difference (end - begin)
    printf("Time elapsed is %d seconds", (end - begin));

    printf("Integer K: %llu\n", number);
    long double loong = 1897456465465465465489789798798798754.0;
    return 0;
}



