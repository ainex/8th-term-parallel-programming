cmake_minimum_required(VERSION 3.14)
project(lab03_cpu C)

set(CMAKE_C_STANDARD 99)

add_executable(lab03_cpu main.c)