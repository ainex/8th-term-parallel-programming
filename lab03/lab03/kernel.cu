
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include "math.h"
#include "stdio.h"
#include <time.h> 


void findNumbersOnCPU(unsigned long long number, int numbersToFind);
cudaError_t checkWithCuda(unsigned long long number, unsigned int numbersToFind);


__global__ void checkKernel(int *c, const int *a)
{
	int i = threadIdx.x;

	unsigned long long number = a[i];

	//find the sum of the squares of digits
	int sum = 0;
	int digit = 0;
	unsigned long long dividend = number;
	while (dividend > 0) {
		digit = dividend % 10;
		sum += digit * digit;
		dividend = dividend / 10;

	}
	//check if sum is the full square
	int root = (int)sqrt((float)sum);
	if (root * root == sum) {
		c[i] = number;
	}
	else {
		c[i] = 0;
	};

}

int main()
{
	const int numbersToFind = 1024 * 1024 * 100;
		
	printf("For any given integer N it finds next integer M,\nso that the sum of the squares of its decimal digits is the full square\n");
	printf("Enter an integer N:\n");

	unsigned long long number = 0;

	scanf("%llu", &number);
	printf("N:%llu\n", number);

	clock_t begin = clock();

	findNumbersOnCPU(number, numbersToFind);

	clock_t end = clock();
	double elapsed = (double)(end - begin) / CLOCKS_PER_SEC;
	printf("Time elapsed is %f seconds\n", elapsed);


	begin = clock();

	//Find numbers in parallel.
	cudaError_t cudaStatus = checkWithCuda(number, numbersToFind);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "addWithCuda failed!");
		return 1;
	}


	// cudaDeviceReset must be called before exiting in order for profiling and
	// tracing tools such as Nsight and Visual Profiler to show complete traces.
	cudaStatus = cudaDeviceReset();
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaDeviceReset failed!");
		return 1;
	}

	end = clock();
	elapsed = (double)(end - begin) / CLOCKS_PER_SEC;
	printf("Time elapsed is %f seconds\n", elapsed);

	return 0;
}

void findNumbersOnCPU(unsigned long long number, int numbersToFind)
{
	int foundNumbersTotal = 0;
	int size = 1024;
	while (foundNumbersTotal < numbersToFind) {

		unsigned long long c[1024] = { 0 };
		unsigned long long a[1024];
		//prepare input array with numbers to check
		for (int i = 0; i < size; i++) {
			number++;
			a[i] = number;
		}

		int notFound = 1;
		for (int i = 0; i < size; i++) {
			//find the sum of the squares of digits
			int sum = 0;
			int digit = 0;

			unsigned long long dividend = a[i];

			while (dividend > 0) {
				digit = dividend % 10;
				sum += digit * digit;
				dividend = dividend / 10;
			}

			int root = (int)sqrt(sum);
			
			if (root * root == sum) {
				c[i] = a[i];
			};

		}

		// calculate the amount of found numbers
		for (int i = 0; i < size; i++) {
			if (c[i] != 0) foundNumbersTotal++;
		}

	}
}

// Helper function for using CUDA to check vectors of numbers in parallel.
cudaError_t checkWithCuda(unsigned long long number, unsigned int numbersToFindCount)
{
	int *dev_a = 0;
	int *dev_c = 0;
	cudaError_t cudaStatus;
	int size = 1024;

	clock_t s01 = clock();
	// Choose which GPU to run on, change this on a multi-GPU system.
	cudaStatus = cudaSetDevice(0);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaSetDevice failed!  Do you have a CUDA-capable GPU installed?");
		goto Error;
	}

	// Allocate GPU buffers for vectors (one input, one output).
	cudaStatus = cudaMalloc((void**)&dev_c, size * sizeof(int));
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMalloc failed!");
		goto Error;
	}

	cudaStatus = cudaMalloc((void**)&dev_a, size * sizeof(int));
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMalloc failed!");
		goto Error;
	}

	clock_t s02 = clock();
	double elapsedinit = (double)(s02-s01) / CLOCKS_PER_SEC;
	printf("Time elapsed CUDA device init and memory is %f seconds\n", elapsedinit);
	
	int foundNumbersTotal = 0;
	while (foundNumbersTotal < numbersToFindCount) {
		clock_t s1 = clock();
		int c[1024] = { 0 };
		int a[1024];
		//prepare input array with numbers to check
		for (int i = 0; i < size; i++) {
			number++;
			a[i] = number;
		}

				
		// Copy input vectors from host memory to GPU buffers.
		cudaStatus = cudaMemcpy(dev_a, a, size * sizeof(int), cudaMemcpyHostToDevice);
		if (cudaStatus != cudaSuccess) {
			fprintf(stderr, "cudaMemcpy failed!");
			goto Error;
		}

		// Launch a kernel on the GPU with one thread for each element.
		checkKernel << <1, size >> > (dev_c, dev_a);


		// Check for any errors launching the kernel
		cudaStatus = cudaGetLastError();
		if (cudaStatus != cudaSuccess) {
			fprintf(stderr, "checkKernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
			goto Error;
		}

		// cudaDeviceSynchronize waits for the kernel to finish, and returns
		// any errors encountered during the launch.
		cudaStatus = cudaDeviceSynchronize();
		if (cudaStatus != cudaSuccess) {
			fprintf(stderr, "cudaDeviceSynchronize returned error code %d after launching checkKernel!\n", cudaStatus);
			goto Error;
		}

		// Copy output vector from GPU buffer to host memory.
		cudaStatus = cudaMemcpy(c, dev_c, size * sizeof(int), cudaMemcpyDeviceToHost);
		if (cudaStatus != cudaSuccess) {
			fprintf(stderr, "cudaMemcpy failed!");
			goto Error;
		}

		// calculate the amount of found numbers
		for (int i = 0; i < size; i++) {
			if (c[i] != 0) foundNumbersTotal++;
		}

		clock_t s5 = clock();
		double elapsed = (double)(s5 - s1) / CLOCKS_PER_SEC;
		//printf("Time elapsed CUDA total is %f seconds\n", elapsed);

	}


Error:
	cudaFree(dev_c);
	cudaFree(dev_a);

	return cudaStatus;
}


