#include <stdio.h>
#include <mpi.h>
#include <chrono>
#include <math.h>
#include <stdlib.h>

using namespace std::chrono;

void printArray(int *array, int size);

void initArray(int *array, int size, int value);

int sumArray(const int *array, int size);

int findAddendums(int number, int parts, int minAddendumLimit, int maxAddendumLimit, int maxAddendum);

bool increase(int *addendums, int size, int maxAddendum, int limit);

int main(int argc, char *argv[]) {
    int rank;
    int size;
    double startWTime = 0.0;
    double endWTime = 0.0;

    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    printf("Hello from process %d of %d\n", rank, size);
    fflush(stdout);
    int result = 10;

    int number = 0;
    int parts = 0;
    if (rank == 0) {
        printf("Enter the number and addendums' amount: (i.e. 30 5)\n");
        fflush(stdout);
        scanf("%d %d", &number, &parts);
        startWTime = MPI_Wtime();
        result = number + parts;
    }
    int input[] = {number, parts};
    MPI_Bcast(&input, 2, MPI_INT, 0, MPI_COMM_WORLD);
    if (rank != 0) {
        number = input[0];
        parts = input[1];
//        printf("In process %d input %d %d\n", rank, number, parts);
//        fflush(stdout);
    }

    // divide to several ranks
    int maxAddendum = number - (parts - 1);
    int step = maxAddendum / size;
    int min = 1 + step * rank;
    int max = step * (rank + 1);
    if (size - 1 == rank) {
        max = maxAddendum;
    }

//    printf("For process %d min: %d max:%d\n", rank, min, max);
//    fflush(stdout);

    result = findAddendums(number, parts, min, max, maxAddendum);

    printf("Found %d combinations at %d process\n", result, rank);
    fflush(stdout);

    int totalResult = 0;
    MPI_Reduce(&result, &totalResult, 1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);

    if (rank == 0) {
        endWTime = MPI_Wtime();
        printf("Found %d combinations\n", totalResult);
        printf("wall clock time = %f\n", endWTime - startWTime);
        fflush(stdout);
    }

    MPI_Finalize();

}

int findAddendums(int number, int parts, int minAddendumLimit, int maxAddendumLimit, int maxAddendum) {
    int total = 0;
    int addendums[parts] = {0};
    initArray(addendums, parts, 1);
    addendums[0] = minAddendumLimit;
    // for number = 6 and parts = 3 the max value of addendum  =  (6 - (3-1)) = 4
    // because it can be represented as [1  1  4]

    bool hasNextValue = true;
    while (hasNextValue) {
        //calculate sum
        int sum = sumArray(addendums, parts);
        if (sum == number) {
            total++;
            //printArray(addendums, parts);
        }
        //next value
        hasNextValue = increase(addendums, parts, maxAddendum, maxAddendumLimit);
    }
    return total;


}

bool increase(int *addendums, int size, int maxAddendum, int limit) {
    bool increased = false;
    int position = size - 1;
    while (!increased) {
        if (position == 0 && addendums[position] == limit) {
            return false;
        }
        if (addendums[position] < maxAddendum) {
            addendums[position] = addendums[position] + 1;
            increased = true;
        } else {
            addendums[position] = 1;
            position--;
            if (position < 0) return false;
        }
    }
    return true;
}

void printArray(int *array, int size) {
    for (int i = 0; i < size; i++) {
        printf("%d ", array[i]);
    }
    printf("\n");
}

void initArray(int *array, int size, int value) {
    for (int i = 0; i < size; i++) {
        array[i] = value;
    }
}

int sumArray(const int *array, int size) {
    int sum = 0;
    for (int i = 0; i < size; i++) {
        sum += array[i];
    }
    return sum;
}
