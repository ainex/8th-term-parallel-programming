#include <stdio.h>
#include <omp.h>
#include <chrono>
#include <iostream>
#include<string.h>

using namespace std;
using namespace std::chrono;

int size = 0;
bool parallel = false;

int omp_thread_count();

int omp_get_step();

int **readMatrixFromFile(FILE *fd, int &size);

void writeResultToFile(int *coefficients, int size);

int getTrace(int **matrix, int size);

int **calculateMatrixB(int **matrixA, int size, int q);

int **calculateMatrixANext(int **matrixA, int **matrixB, int size);

int calculateCoefficient(int iteration, int **matrixANext, int size);

int **createZeroMatrix(int size);

int **copyMatrix(int **matrixSource, int size);


int main(int argc, char **argv) {
    for (int i = 0; i < argc; ++i) {
        if (strcmp(argv[i], "-p") == 0) {
            parallel = true;
        }
    }
#ifdef _OPENMP
    printf("Version OpenMP is %d\n", _OPENMP);
#endif
    printf("Max threads: %d\n", omp_get_max_threads());
    printf("Parallel mode: %s\n", parallel ? "ON" : "OFF");

    FILE *fd = fopen("matrix.txt", "rt");

    int **matrixA = readMatrixFromFile(fd, size);

    int *coefficients = new int[size + 1];
    coefficients[0] = 1;

    //calculate the coefficients of the characteristics polynomial of a square matrix
    //using the Faddeev–LeVerrier algorithm -  a recursive method
    //q1 = Tr A;  Bi = Ai − qi*I;  Ai+1 = A*Bi;  qi+1 = (1/i+1) * Tr Ai+1
    auto start = system_clock::now();

    //q1 = Tr A
    int q = getTrace(matrixA, size);
    coefficients[1] = q;

    int **matrixANext = copyMatrix(matrixA, size);
    int **matrixB;

    for (int index = 2; index <= size; index++) {

        matrixB = calculateMatrixB(matrixANext, size, q);
        matrixANext = calculateMatrixANext(matrixA, matrixB, size);
        q = calculateCoefficient(index, matrixANext, size);
        coefficients[index] = q;
    }

    writeResultToFile(coefficients, size + 1);

    auto end = system_clock::now();
    auto delta = duration_cast<milliseconds>(end - start).count() / 1000.0;
    printf("\nExecution time: %f", delta);

    return 0;
}

int **readMatrixFromFile(FILE *fd, int &size) {
    int rows = 0;
    int columns = 0;
    int element = 0;
    fscanf(fd, "%d", &rows);
    fscanf(fd, "%d", &columns);
    if (rows == columns) {
        size = rows;
    }
    int **matrix = new int *[size];  // dynamic array of pointers
    for (int i = 0; i < size; i++) {
        matrix[i] = new int[size];  // dynamic lines
        for (int j = 0; j < size; j++) {
            fscanf(fd, "%d", &element);
            matrix[i][j] = element;
        }
    }
    fclose(fd);

    return matrix;
}

int getTrace(int **matrix, int size) {
    int step = omp_get_step();
    int trace = 0;
    int index = 0;
#pragma omp parallel if(parallel) private(index) reduction(+: trace)
    {
        index = omp_get_thread_num();
        while (index < size) {
            trace += matrix[index][index];
            index += step;
        }
    }
    return trace;
}

// Bi = Ai − qi*I,
int **calculateMatrixB(int **matrixA, int size, int q) {
    int **matrixB = createZeroMatrix(size);
    for (int i = 0; i < size; i++)
        for (int j = 0; j < size; j++)
            matrixB[i][j] = (i == j) ? matrixA[i][j] - q : matrixA[i][j];
    return matrixB;
}

// Ai+1 = A*Bi,
int **calculateMatrixANext(int **matrixA, int **matrixB, int size) {
    int **matrixResult = createZeroMatrix(size);
    int step = omp_get_step();
    int index = 0;
#pragma omp parallel if(parallel) private(index)
    {
        index = omp_get_thread_num();
        while (index < size) {
            for (int j = 0; j < size; j++) {
                for (int k = 0; k < size; k++) {
                    matrixResult[index][j] += matrixA[index][k] * matrixB[k][j];
                }
            }
            index += step;
        }
    }
    return matrixResult;
}

// qi+1 = (1/i+1) * Tr Ai+1
int calculateCoefficient(int iteration, int **matrixANext, int size) {
    return getTrace(matrixANext, size) / (iteration);
}

int **createZeroMatrix(int size) {
    int **matrix = new int *[size];
    for (int i = 0; i < size; i++) {
        matrix[i] = new int[size];
        for (int j = 0; j < size; j++) {
            matrix[i][j] = 0;
        }
    }
    return matrix;
}

int **copyMatrix(int **matrixSource, int size) {
    int **matrixCopy = createZeroMatrix(size);
    for (int i = 0; i < size; i++)
        for (int j = 0; j < size; j++)
            matrixCopy[i][j] = matrixSource[i][j];
    return matrixCopy;
}

int omp_thread_count() {
    int n = 0;
#pragma omp parallel reduction(+:n)
    n += 1;
    return n;
}

int omp_get_step() {
    return parallel ? omp_thread_count() : 1;
}

void writeResultToFile(int *coefficients, int size) {
    FILE *fd = fopen("result.txt", "w");
    for (int i = 0; i < size; i++) {
        fprintf(fd, "%d ", coefficients[i]);
    }
}
